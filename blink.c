#include <stdlib.h>
#include "hardware/adc.h"
#include "hardware/flash.h"
#include "pico/stdlib.h"
#include "sha256.h" // for random
#include "solo/fido2/ctaphid.h"
#include "solo/fido2/device.h"
#include "tusb.h"

int g_bootCnt = 0;
uint64_t loopCount = 0;


// Invoked when received GET_REPORT control request
// Application must fill buffer report's content and return its length.
// Return zero will cause the stack to STALL request
uint16_t tud_hid_get_report_cb(uint8_t instance,uint8_t report_id, hid_report_type_t report_type, uint8_t *buffer, uint16_t reqlen) {
    // TODO not Implemented
    (void) report_id;
    (void) report_type;
    (void) buffer;
    (void) reqlen;

    return 0;
}

struct {
	uint8_t report_id;
	hid_report_type_t report_type;
	uint8_t *buffer;
	uint16_t bufsize;
	
	uint16_t bufalloc;
	int recvCnt;
	int ackCnt;
} cur_set_report = { 0 };
// Invoked when received SET_REPORT control request or
// received data on OUT endpoint ( Report ID = 0, Type = 0 )
void tud_hid_set_report_cb(uint8_t instance, uint8_t report_id, hid_report_type_t report_type, uint8_t const *buffer, uint16_t bufsize) {
    if (cur_set_report.bufalloc < bufsize) {
		free(cur_set_report.buffer);
		cur_set_report.buffer = (uint8_t*)malloc(bufsize);
		cur_set_report.bufalloc = bufsize;
	}
	cur_set_report.bufsize = bufsize;
	memcpy(cur_set_report.buffer, buffer, bufsize);
	cur_set_report.report_id = report_id;
	cur_set_report.report_type = report_type;
	cur_set_report.recvCnt++;
}


// solo device.h impl begin
uint32_t millis() { return time_us_32()/1000; }
void usbhid_send(uint8_t * msg64b) {
	tud_hid_n_report(0/*instance*/, 0/*report_id*/, msg64b, 64);
}

// PICO_FLASH_SIZE_BYTES /* 0x200000 /* 2MB*/
// FLASH_SECTOR_SIZE 4k 0x1000
#define BOOT_CNT_OFFSET (PICO_FLASH_SIZE_BYTES - 2*FLASH_SECTOR_SIZE )

struct bootInfo {
	int cnt;
	int magic;
	uint32_t nv_ctap;
} *gp_bootInfo = (struct bootInfo*)(XIP_BASE+BOOT_CNT_OFFSET);

#define BOOT_CNT_MAGIC (0x50c14f65 ^ sizeof(struct bootInfo))

#define AUTH_STATE_OFFSET (PICO_FLASH_SIZE_BYTES - 3*FLASH_SECTOR_SIZE)

int authenticator_read_state(AuthenticatorState * s) {
	memcpy(s, 
		(void*)(XIP_BASE + AUTH_STATE_OFFSET), 
		sizeof(AuthenticatorState)); // 204 bytes
		
	// don't go past end of pi pico flash
	assert(sizeof(AuthenticatorState) < 512);
	
	// don't collide with program
	assert(__flash_binary_end < XIP_BASE + AUTH_STATE_OFFSET); 
}

void authenticator_write_state(AuthenticatorState * s) {
	// https://raspberrypi.github.io/pico-sdk-doxygen/group__hardware__flash.html
	// page size 256 bytes 0x0100
	// sector size is 4k 0x1000
	
	uint8_t page[FLASH_PAGE_SIZE];
    memset(page,0,sizeof(page));
	memcpy(page, s, sizeof(AuthenticatorState));
	
	flash_range_erase(AUTH_STATE_OFFSET, FLASH_SECTOR_SIZE);
	flash_range_program(AUTH_STATE_OFFSET, page, FLASH_PAGE_SIZE);
	
} // */

//int device_is_button_pressed(); // TODO
//void device_wink(); // should not block

static uint32_t rngSeed[8] = { 0 };
int ctap_generate_rng(uint8_t * dst, size_t num) {
	adc_select_input(4);
	rngSeed[4] ^= loopCount; // add entropy
	
	rngSeed[5] ^= millis();
	rngSeed[6] ^= (int)dst;
	rngSeed[7] ^= num;
	
	int generated = 0;
	while (num > generated) {
		rngSeed[6] ^= generated;
		rngSeed[3] ^= adc_read(); // add entropy
		
		SHA256_CTX ctx;
		sha256_init(&ctx);
		sha256_update(&ctx, (BYTE*)rngSeed, 32);
		sha256_final(&ctx, (BYTE*)rngSeed);
		int toCopy = num - generated;
		if (toCopy > 32) { toCopy = 32; }
		memcpy(dst+generated, rngSeed, toCopy);
		generated += 32;
	}
	rngSeed[6] ^= generated + 1;
	rngSeed[3] ^= adc_read(); // add entropy

	SHA256_CTX ctx;
	sha256_init(&ctx);
	sha256_update(&ctx, (BYTE*)rngSeed, 32);
	sha256_final(&ctx, (BYTE*)rngSeed);
	
	return 1; // success
} // */

uint32_t ctap_atomic_count(uint32_t amount) {
	
	if (amount) {
		uint8_t *page=(uint8_t*)malloc(FLASH_PAGE_SIZE);
		struct bootInfo *bi2 = (struct bootInfo*)page;
		memcpy(bi2, gp_bootInfo, sizeof(struct bootInfo));
		bi2->nv_ctap += amount;
		flash_range_erase(BOOT_CNT_OFFSET, FLASH_SECTOR_SIZE);
		flash_range_program(BOOT_CNT_OFFSET, page, FLASH_PAGE_SIZE);
		free(page);
	}
	return gp_bootInfo->nv_ctap;
}

void hexDump (void *addr, int len) {
    int i;
    char buff[17];
    char *pc = (char*)addr;
    const char * HEX="0123456789abcdef";
    for (i = 0; i < len; i++) {
        if ((i % 16) == 0) {
            if (i != 0) {
			    tud_cdc_write_str(" ");
			    tud_cdc_write_str(buff);
			    tud_cdc_write_str("\n");
                //printf ("  %s\n", buff);
			}
			buff[0]='\0';
            sprintf (buff,"%04x ", i);
            tud_cdc_write_str(buff);
			buff[0]='\0';
        }
        tud_cdc_write_char(HEX[pc[i]>>4]);
        tud_cdc_write_char(HEX[pc[i]&0xf]);
        //printf (" %02x", pc[i]);
        
        if ((pc[i] < 0x20) || (pc[i] > 0x7e))
            buff[i % 16] = '.';
        else
            buff[i % 16] = pc[i];
        buff[(i % 16) + 1] = '\0';
    }
    /*while ((i % 16) != 0) {
        printf ("   ");
        i++;
    } // */
    // printf ("  %s\n", buff);
    tud_cdc_write_str(" ");
    tud_cdc_write_str(buff);
    tud_cdc_write_str("\n");
}


//--------------------------------------------------------------------+
// USB CDC
//--------------------------------------------------------------------+
char cmd[64];
int cmdIdx=0;
void cdc_task(void)
{
    // connected and there are data available
    if ( tud_cdc_available() )
    {
        // read datas
        char pre='\0',buf[64],post='\0';
        uint32_t count = tud_cdc_read(buf, sizeof(buf));
        
		// typically only reads 1
		if (count > 1) {
			char out[64];
			out[0]='\0';
			snprintf(out, sizeof(out), "%dread\r\n", count);
			tud_cdc_write_str(out);
		}
		
		buf[count] = '\0';
		tud_cdc_write_str(buf);
		
		cmd[cmdIdx++] = buf[0];
		if (buf[0] == ';') {
			tud_cdc_write_str("begin cmd\r\n");
			tud_cdc_write_str(cmd);
			tud_cdc_write_str(" end cmd\r\n"); 
			cmd[--cmdIdx] = '\0';
			int addr;
			sscanf(cmd, "%x", &addr);
			hexDump((void*)addr, 256);
			tud_cdc_write_flush();
		}
    }
    
    static int last = 0;
    int cur_us = time_us_32();
    if (cur_us - last > 1000111) {	
		char out[128];		
		out[0]='\0';
		snprintf(out, sizeof(out), "begin%dus %dloop %dboot %dhid %dend\r\n", 
			cur_us, (int)loopCount, g_bootCnt, cur_set_report.recvCnt, cur_us);
		tud_cdc_write_str(out);
		tud_cdc_write_flush();
		last = cur_us;
	} // */
}

void rng_init(int moreSeed) {
	flash_get_unique_id((uint8_t*)&rngSeed); // 64-bit
	rngSeed[2] = time_us_32();

    adc_init(); // for rand // temp
	adc_select_input(4);
	rngSeed[3] = adc_read();
	
	rngSeed[4] = moreSeed;
	
	//rngSeed[5] = __flash_binary_end;
}

int main() {
    stdio_init_all();
    tusb_init();
    
	uint8_t *page=(uint8_t*)malloc(FLASH_PAGE_SIZE);
	memset(page,0,sizeof(page));
	struct bootInfo *bi2 = (struct bootInfo*)page;
	if (gp_bootInfo->magic == BOOT_CNT_MAGIC) {
		memcpy(bi2, gp_bootInfo, sizeof(struct bootInfo));
	}
	bi2->cnt += 1;
	g_bootCnt = bi2->cnt;
	flash_range_erase(BOOT_CNT_OFFSET, FLASH_SECTOR_SIZE);
	flash_range_program(BOOT_CNT_OFFSET, page, FLASH_PAGE_SIZE);
	free(page);

    rng_init(g_bootCnt);


    uint8_t hidmsg[64]; // solo
    uint32_t t1 = 0;// solo

    memset(hidmsg,0,sizeof(hidmsg));// solo

    while(true) {
		loopCount++;
        tud_task();
        cdc_task(); // for debug
		
		if (cur_set_report.recvCnt > cur_set_report.ackCnt) {
			cur_set_report.ackCnt++;
			
			int sz = cur_set_report.bufsize;
			if (sz > sizeof(hidmsg)) {
				char out[64];
				snprintf(out, sizeof(out), " clipping hid buffer %d\r\n", sz);
				tud_cdc_write_str(out);
				
				sz = sizeof(hidmsg);
			}
			memcpy(hidmsg, cur_set_report.buffer, sz);

            ctaphid_handle_packet(hidmsg);      // pass into libsolo!
            memset(hidmsg, 0, sizeof(hidmsg));// solo
        }

        ctaphid_check_timeouts();// solo

    } // end while true
}
